import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StoreModule } from '@ngrx/store';

import { SharedModule } from '../shared/shared.module';

import { userReducer } from './state/user.reducer';
import { LoginComponent } from './login.component';

const userRoutes: Routes = [
  { path: '', component: LoginComponent}
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(userRoutes),
    StoreModule.forFeature('user',userReducer)
  ],
  declarations: [
    LoginComponent
  ]
})
export class UserModule { }
