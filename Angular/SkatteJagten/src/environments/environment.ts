// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  API_URL: 'https://localhost:44322/',
  firebase: {
    apiKey: "AIzaSyAq5kWon0-F9OpomCZd_AmtOUmUDwz8W4g",
    authDomain: "skattejagten-df1fe.firebaseapp.com",
    databaseURL: "xxxxxxxxxxxxxxxxxxxxxxxx",
    projectId: "skattejagten-df1fe",
    storageBucket: "skattejagten-df1fe.appspot.com",
    messagingSenderId: "436550473075",
    appId: "1:436550473075:web:9e8735f69d2fba683a2106",
    measurementId: "xxxxxxxxxxxxxxxx"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
