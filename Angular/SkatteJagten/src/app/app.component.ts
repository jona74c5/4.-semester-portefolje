import { Component } from '@angular/core';

@Component({
  selector: 'sj-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'SkatteJagten';
}
