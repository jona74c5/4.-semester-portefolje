import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { IProfile } from 'src/app/models/profile';

@Injectable({
  providedIn: 'root',
})
export class ProfileService {
  constructor(private httpClient: HttpClient) {}
  private apiURL = 'https://team5api.azurewebsites.net/v1.0/profiles/';


  getProfile(id: string): Observable<IProfile> {
    return this.httpClient
      .get<IProfile>(this.apiURL + id)
      .pipe(catchError(this.handleError));
  }

  addProfile(id: string, userName: string) {
    return this.httpClient
      .post(this.apiURL, { "id": id, "userName": userName })
      .pipe(catchError(this.handleError));
  }

  updateProfile(profile: IProfile): Observable<IProfile> {
    return this.httpClient
    .put<IProfile>(this.apiURL, profile)
    .pipe(catchError(this.handleError));
  }

  private handleError(err: HttpErrorResponse) {
    //normalt ville man sende fejl beskeder til en anden logging infrastruktur istedet for at printe til konsol.
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      //En client-side eller netværks error opstod. Håndter den efter behov.
      errorMessage = `An error occured: ${err.message}`;
    } else {
      //Backenden returnerede en unsuccesfull response kode.
      //Response body indeholder informationer om hvad der kan være gået galt.
      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }
}
