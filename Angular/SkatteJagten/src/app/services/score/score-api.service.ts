import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import { ITotalScoreForUser } from '../../models/total-score-for-user'

@Injectable({
  providedIn: 'root',
})
export class ScoreAPIService {
  constructor(private httpClient: HttpClient) {}
  private ApiUrl = 'https://team5api.azurewebsites.net/v1.0/scores/';

  getTotalScoreForUser(name: string) {

    return this.httpClient.get<ITotalScoreForUser>(`Scores/TotalScore/${name}`);

  }

  getAllTotalScores(): Observable<ITotalScoreForUser[]>{
    return this.httpClient.get<ITotalScoreForUser[]>(this.ApiUrl+'AllTotalScores')
      .pipe(tap(data => console.log('All', JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  private handleError(err: HttpErrorResponse){
    //normalt ville man sende fejl beskeder til en anden logging infrastruktur istedet for at printe til konsol.
    let errorMessage = '';
    if(err.error instanceof ErrorEvent){
      //En client-side eller netværks error opstod. Håndter den efter behov.
      errorMessage = `An error occured: ${err.message}`;
    } else{
      //Backenden returnerede en unsuccesfull response kode.
      //Response body indeholder informationer om hvad der kan være gået galt.
      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`
    }
    console.error(errorMessage);
    return throwError(errorMessage);

  }
}
