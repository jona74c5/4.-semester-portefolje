import { TestBed } from '@angular/core/testing';

import { ScoreAPIService } from './score-api.service';

describe('ScoreAPIService', () => {
  let service: ScoreAPIService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ScoreAPIService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
