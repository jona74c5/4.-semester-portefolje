import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { IProfile } from 'src/app/models/profile';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ProfileService } from 'src/app/services/profile/profile.service';

@Component({
  selector: 'sj-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit, OnDestroy {
  profile: IProfile | undefined;
  errorMessage = '';
  sub!: Subscription;

  constructor(
    public authService: AuthService,
    private profileService: ProfileService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    const userData = this.authService.userData as User;
    this.getProfile(userData.uid)
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  onBack(): void {
    this.router.navigate(['/profile']);
  }

  getProfile(id: string): void {
    this.sub = this.profileService.getProfile(id).subscribe({
      next: (profile) => (this.profile = profile),
      error: (err) => (this.errorMessage = err)
    });
  }

  putProfile(): void {
    if(this.profile == undefined){
      return;
    }
    this.profileService.updateProfile(this.profile)
    .pipe(take(1)).subscribe({
      next: () => this.router.navigate(['/profile']),
      error: (err) => (this.errorMessage = err)
    });
  }


}
