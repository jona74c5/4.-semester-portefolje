import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ApiurlInterceptor } from 'src/app/interceptors/apiurl.interceptor';
import { ITotalScoreForUser } from 'src/app/models/total-score-for-user';
import { ScoreAPIService } from 'src/app/services/score/score-api.service';

@Component({
  templateUrl: './scoreboard.component.html',
  styleUrls: ['./scoreboard.component.css']
})
export class ScoreboardComponent implements OnInit, OnDestroy {

  errorMessage: string = '';
  sub!: Subscription;

  private _scoreFilter: string = '';
  get scoreFilter(): string {
    return this._scoreFilter;
  }
  set scoreFilter(value: string){
    this._scoreFilter = value;
    this.filteredScores = this.performFilter(value)
  }

  filteredScores: ITotalScoreForUser[] = [];

  scores: ITotalScoreForUser[] = [];

  constructor(private scoreApiService: ScoreAPIService) { }

  performFilter(filterBy: string): ITotalScoreForUser[]{
    filterBy = filterBy.toLocaleLowerCase();
    return this.scores.filter((totalScoreForUser: ITotalScoreForUser) =>
    totalScoreForUser.userName.toLocaleLowerCase().includes(filterBy));
  }

  ngOnInit(): void {
    this.sub = this.scoreApiService.getAllTotalScores().subscribe({
      next: (scores: ITotalScoreForUser[]) => {
        this.scores = scores;
        this.filteredScores = this.scores;
      },
      error: err => this.errorMessage = err
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

}
