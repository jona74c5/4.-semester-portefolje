import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { IProfile } from 'src/app/models/profile';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ProfileService } from 'src/app/services/profile/profile.service';

@Component({
  selector: 'sj-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})

export class ProfileComponent implements OnInit, OnDestroy {
  profile: IProfile | undefined;
  errorMessage = '';
  sub!: Subscription;

  constructor(
    public authService: AuthService,
    private profileService: ProfileService
  ) { }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  ngOnInit(): void {
    const userData = this.authService.userData as User;
    this.getProfile(userData.uid)
  }
 /*
  getProfile(id: string): void {
    this.profileService.getProfile(id).pipe(take(1)).subscribe({
      next: (profile) => (this.profile = profile),
      error: (err) => (this.errorMessage = err),
    });
  }
 */

  getProfile(id: string): void {
    this.sub = this.profileService.getProfile(id).subscribe({
      next: (profile) => (this.profile = profile),
      error: (err) => (this.errorMessage = err),
    });
  }
}


