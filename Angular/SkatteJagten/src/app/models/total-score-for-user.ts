export interface ITotalScoreForUser {
  userName: string;
  points: number;
}
