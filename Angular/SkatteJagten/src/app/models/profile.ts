export interface IProfile {
  id: string;
  userName: string;
  fullName: string;
  department: string;
  profileText: string;
  picture: string;
}
