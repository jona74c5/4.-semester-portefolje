import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpHeaders
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class ApiurlInterceptor implements HttpInterceptor {

  constructor() {}
  stdHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Accept': 'application/json' });

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request);
  }
}
