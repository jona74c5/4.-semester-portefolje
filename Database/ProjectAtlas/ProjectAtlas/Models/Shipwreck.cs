﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectAtlas.Models
{
    [BsonIgnoreExtraElements] //Ignorer alle elementer der ikke kan serializes.
    public class Shipwreck
    {
        public ObjectId Id { get; set; }
        [BsonElement("feature_type")]
        public string FeatureType  { get; set; }
        [BsonElement("chart")]
        public string Chart { get; set; }
        [BsonElement("latdec")]
        public double Lattitude { get; set; }
        [BsonElement("londec")]
        public double Longitude { get; set; }

        /*
        [BsonExtraElements] //Hvis der findes elementer der ikke kan serializes, smides de her som key-value pair. Ikke smart da der indlæses unødvendig data.
        public object[] Bucket { get; set; }
        */
    }
}
