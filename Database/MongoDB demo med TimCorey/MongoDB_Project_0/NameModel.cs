﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace MongoDB_Project_0
{
    [BsonIgnoreExtraElements] //Denne attribut sørger for at hvis der er elementer i en kollektion der ikke er repræsenteret i modellen, ignoreres de.
    public class NameModel
    {
        [BsonId]
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
