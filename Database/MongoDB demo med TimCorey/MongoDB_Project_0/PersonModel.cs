﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace MongoDB_Project_0
{
    public class PersonModel
    {
        [BsonId] // dette bliver _id, MongoDB laver et null check på recorden når kaldet laves, hvis true er det et insert statement, hvis ikke, er det et update.
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public AddressModel PrimaryAddress { get; set; }

        [BsonElement("dob")] //Navngiver elementet i databasen til det angivne. Her "dob" istedet for DateOfBirth
        public DateTime DateOfBirth { get; set; }
    }
}
