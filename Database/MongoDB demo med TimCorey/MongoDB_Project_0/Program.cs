﻿using System;

namespace MongoDB_Project_0
{
    class Program
    {
        static void Main(string[] args)
        {
            MongoCRUD db = new MongoCRUD("AddressBook");

            //PersonModel person = new PersonModel
            //{
            //    FirstName = "Jens",
            //    LastName = "Jensen",
            //    PrimaryAddress = new AddressModel
            //    {
            //        StreetAddress = "Vejgade 4",
            //        City = "Søllested",
            //        State = "Syddanmark",
            //        ZipCode = "7500"
            //    }
            //};

            //db.InsertRecord("Users", person); //indsætter den ovenover instantierede personmodel.
            //db.InsertRecord("Users", new PersonModel {FirstName = "Ramsus", LastName = "Christiansen" }); //Gør det samme som linjen lige over, men opretter den direkte i parameteren.

            //var recs = db.LoadRecords<PersonModel>("Users"); //Indlæser alle records fra db i kollektionen "users"

            //foreach (var rec in recs)
            //{
            //    Console.WriteLine($"{rec.Id}: {rec.FirstName} {rec.LastName}");

            //    if (rec.PrimaryAddress != null)
            //    {
            //        Console.WriteLine($"{rec.PrimaryAddress.StreetAddress}, {rec.PrimaryAddress.ZipCode} {rec.PrimaryAddress.City}");
            //    }
            //    Console.WriteLine();
            //}

            //var FoundRecord = db.LoadRecordById<PersonModel>("Users", new Guid("3224ae20-b531-4592-9964-bd9d54d1438a"));

            //FoundRecord.DateOfBirth = new DateTime(1987,03,22,0,0,0,DateTimeKind.Utc);
            //FoundRecord.PrimaryAddress = new AddressModel
            //{
            //    StreetAddress = "Otte Ruds Vej 15",
            //    City = "Odense SØ",
            //    State = "Syddanmark",
            //    ZipCode = "5220"
            //};
            //db.UpsertRecord("Users", FoundRecord.Id, FoundRecord); //opdatere den fundne record med den opdaterede datetime instantieret ovenfor samt den nye addressModel.

            //db.DeleteRecords<PersonModel>("Users", FoundRecord.Id); //Sletter den fundne record.

            var recs = db.LoadRecords<NameModel>("Users"); //Et eksempel på hvordan man kan bruge en anden model til at returnere data fra DB.

            foreach (var rec in recs)
            {
                Console.WriteLine($"{rec.FirstName} {rec.LastName}");
                Console.WriteLine();
            }

            Console.ReadLine();
        }
    }
}
